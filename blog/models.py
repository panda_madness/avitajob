from django.db import models

# Create your models here.

class Article(models.Model):
	title = models.CharField(max_length = 100)
	contents = models.TextField()
	excerpt = models.CharField(max_length = 150)
	pub_date = models.DateTimeField()
	is_published = models.BooleanField()

	def __str__(self):
		return self.title