from django.contrib import admin

from .models import Article

# Register your models here.

class ArticleAdmin(admin.ModelAdmin):
	fieldsets = [
		('Title', {'fields': ['title']}),
		('Contents', {'fields': ['contents']}),
		('Excerpt', {'fields': ['excerpt']}),
		('Date and publishing', {'fields': ['pub_date', 'is_published']}),
	]



admin.site.register(Article, ArticleAdmin)
