from django.shortcuts import render
from django.http import HttpResponse
from django.core.urlresolvers import reverse

from .models import Article

# Create your views here.


def index(request):
	main_article = Article.objects.order_by('-pub_date').filter(is_published = True)[0]
	latest_articles = Article.objects.order_by('pub_date').filter(is_published = True)[1:5]
	return render(request, 'blog/homepage.html', {'main_article': main_article, 'latest_articles': latest_articles})
	#return HttpResponse(reverse('blog:article', kwargs = {'article_id': 5}))

def article(request, article_id):
	article = Article.objects.get(pk = article_id)
	return render(request, 'blog/article.html', {'article': article})